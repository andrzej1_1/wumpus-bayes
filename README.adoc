= Wumpus World - bayes network

== Requirements

- python 3.7
- pipenv

== Usage

Before first run you have to install dependencies:

[source,sh]
----
$ pipenv install
----

Then you can run:

[source,sh]
----
$ pipenv run main.py
----

== Example

Let's consider simple _4x4_ board:

image::doc/board.png[Board]

We represent it with following array:

[source,python]
----
[
    ['?','?','?','?'],
    ['?','?','?','?'],
    ['b','?','?','?'],
    ['_','b','?','?'],
]
----

where each string has following meaning:

- ? - _unexplored_ field
- b - field with _breeze_
- s - field with _stench_
- m - field with both _breeze_ and _stench_
- _ - _empty_ field

We also specify _pit_ and _wumpus_ occurence probability, _20%_ and _5%_ respectively.

After running `main.py` we get probabilites for all _frontiers_:

[source]
----
* pit 0,1 (0.3103448275862072, 0.6896551724137928)
* pit 2,3 (0.3103448275862072, 0.6896551724137928)
* pit 1,2 (0.8620689655172411, 0.1379310344827588)
* breeze 1,3 T
* breeze 0,2 T
* wumpus 0,1 (0.05000000000000021, 0.9499999999999997)
* wumpus 2,3 (0.05000000000000021, 0.9499999999999997)
* wumpus 1,2 (0.05000000000000021, 0.9499999999999997)
----

We can clearly see that occurence of _pit at 1,2_ has _86%_ chance, which is much more than for the other two places (31%).

There is also generated file `graph.pdf` which shows our _bayes network_:

image::doc/graph.png[Graph]
