from pomegranate import *
import itertools


class BoardAnalyzer:

    def __init__(self, board, pit_probability, wumpus_probability):
        self.pit_probability = pit_probability
        self.wumpus_probability = wumpus_probability
        self.network = BayesianNetwork("WumpusNetwork")
        self.node_names = []

        ### Base code for handling pits and breezes

        # Pits are parent nodes
        pits = {}
        pit_nodes = {}
        frontiersCoords = board.getFrontiers()
        for x, y in frontiersCoords:
            pits[(x, y)] = DiscreteDistribution({
                'T': pit_probability,
                'F': (1 - pit_probability)
            })
            node_name = "pit " + str(x) + "," + str(y)
            self.node_names.append(node_name)
            pit_nodes[(x, y)] = State(pits[(x, y)], name=node_name)
            self.network.add_node(pit_nodes[(x, y)])

        # Breeze are dependent on pits
        breezes = {}
        breeze_nodes = {}
        breezesCoords = board.getBreezes()
        for x, y in breezesCoords:
            dependent_pits = []
            dependent_pit_nodes = []
            neighbour_coords = board.getNeighbourCoords(x, y)
            for coord in neighbour_coords:
                if coord in pits:
                    dependent_pits.append(pits[coord])
                    dependent_pit_nodes.append(pit_nodes[coord])
            cond_probabilities = []
            dependent_pits_num = len(dependent_pits)
            for combination in itertools.product(['T', 'F'], repeat=dependent_pits_num):
                comb_list = list(combination)
                if 'T' in combination:
                    cond_probabilities.append(comb_list + ['T', 1.0])
                    cond_probabilities.append(comb_list + ['F', 0.0])
                else:
                    cond_probabilities.append(comb_list + ['T', 0.0])
                    cond_probabilities.append(comb_list + ['F', 1.0])

            breezes[(x, y)] = ConditionalProbabilityTable(cond_probabilities, dependent_pits)
            node_name = "breeze " + str(x) + "," + str(y)
            self.node_names.append(node_name)
            breeze_nodes[(x, y)] = State(breezes[(x, y)], name=node_name)
            self.network.add_node(breeze_nodes[(x, y)])
            for pit_node in dependent_pit_nodes:
                self.network.add_edge(pit_node, breeze_nodes[(x, y)])


        ### Redundant code, copied from above to handle stenches and wumpuses.
        ### Even variable names are not changes, but only node names,
        ### probabilites and few small things.

        # Pits are parent nodes
        pits = {}
        pit_nodes = {}
        frontiersCoords = board.getFrontiers()
        for x, y in frontiersCoords:
            pits[(x, y)] = DiscreteDistribution({
                'T': wumpus_probability,
                'F': (1 - wumpus_probability)
            })
            node_name = "wumpus " + str(x) + "," + str(y)
            self.node_names.append(node_name)
            pit_nodes[(x, y)] = State(pits[(x, y)], name=node_name)
            self.network.add_node(pit_nodes[(x, y)])

        # Breeze are dependent on pits
        breezes = {}
        breeze_nodes = {}
        breezesCoords = board.getStenches()
        for x, y in breezesCoords:
            dependent_pits = []
            dependent_pit_nodes = []
            neighbour_coords = board.getNeighbourCoords(x, y)
            for coord in neighbour_coords:
                if coord in pits:
                    dependent_pits.append(pits[coord])
                    dependent_pit_nodes.append(pit_nodes[coord])
            cond_probabilities = []
            dependent_pits_num = len(dependent_pits)
            for combination in itertools.product(['T', 'F'], repeat=dependent_pits_num):
                comb_list = list(combination)
                if 'T' in combination:
                    cond_probabilities.append(comb_list + ['T', 1.0])
                    cond_probabilities.append(comb_list + ['F', 0.0])
                else:
                    cond_probabilities.append(comb_list + ['T', 0.0])
                    cond_probabilities.append(comb_list + ['F', 1.0])

            breezes[(x, y)] = ConditionalProbabilityTable(cond_probabilities, dependent_pits)
            node_name = "stench " + str(x) + "," + str(y)
            self.node_names.append(node_name)
            breeze_nodes[(x, y)] = State(breezes[(x, y)], name=node_name)
            self.network.add_node(breeze_nodes[(x, y)])
            for pit_node in dependent_pit_nodes:
                self.network.add_edge(pit_node, breeze_nodes[(x, y)])

        ### Common part

        self.network.bake()

        return

    def getNthNodeName(self, nth):
        node_name = self.node_names[nth]
        return node_name

    def probabilites(self):
        # build list of known facts
        # eg. {'breeze 0,2': 'T', 'breeze 1,3': 'T', 'stench 3,2'}
        facts = {}
        for node_name in self.node_names:
            if node_name.startswith('breeze'):
                facts[node_name] = 'T'
            elif node_name.startswith('stench'):
                facts[node_name] = 'T'
        # get network prediction
        probs = self.network.predict_proba(facts)
        return probs

    def plot(self, filename):
        self.network.plot(filename)
        return
