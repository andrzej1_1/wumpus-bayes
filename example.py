from pomegranate import *

# Pits are placed completely random (20%)
pit13 = DiscreteDistribution({'T': 2. / 10, 'F': 8. / 10})
pit22 = DiscreteDistribution({'T': 2. / 10, 'F': 8. / 10})
pit31 = DiscreteDistribution({'T': 2. / 10, 'F': 8. / 10})

# Breeze are dependent on pits
breeze12 = ConditionalProbabilityTable(
    [['F', 'F', 'T', 0.0],
     ['F', 'F', 'F', 1.0],
     ['T', 'T', 'T', 1.0],
     ['T', 'T', 'F', 0.0],
     ['T', 'F', 'T', 1.0],
     ['T', 'F', 'F', 0.0],
     ['F', 'T', 'T', 1.0],
     ['F', 'T', 'F', 0.0]], [pit13, pit22])
breeze21 = ConditionalProbabilityTable(
    [['F', 'F', 'T', 0.0],
     ['F', 'F', 'F', 1.0],
     ['T', 'T', 'T', 1.0],
     ['T', 'T', 'F', 0.0],
     ['T', 'F', 'T', 1.0],
     ['T', 'F', 'F', 0.0],
     ['F', 'T', 'T', 1.0],
     ['F', 'T', 'F', 0.0]], [pit22, pit31])

# Make the nodes
node_pit13 = State(pit13, name="pit13")
node_pit22 = State(pit22, name="pit22")
node_pit31 = State(pit31, name="pit31")
node_breeze12 = State(breeze12, name="breeze12")
node_breeze21 = State(breeze21, name="breeze21")

# Make the bayes net, add the states, and the conditional dependencies.
network = BayesianNetwork("WumpusNetwork")
network.add_nodes(node_pit13, node_pit22, node_pit31, node_breeze12, node_breeze21)
network.add_edge(node_pit13, node_breeze12)
network.add_edge(node_pit22, node_breeze12)
network.add_edge(node_pit22, node_breeze21)
network.add_edge(node_pit31, node_breeze21)
network.bake()

print('Probability of pit13, pit22, pit31')
prob = network.predict_proba({'breeze12': 'T', 'breeze21': 'T'})
print(prob)

print('Saving graph to graph.pdf')
network.plot('./graph.pdf')
