from pomegranate import *
from wumpus import Board
from board_analyzer import BoardAnalyzer


if __name__ == "__main__":

    gold_probability = 0.2
    wumpus_probability = 0.05

    # UNDISCOVERED_FIELD = '?'
    # BREEZE_FIELD = 'b'
    # STENCH_FIELD = 's'
    # BREEZE_N_STENCH_FIELD = 'm'
    # EMPTY_FIELD = '_'
    board = Board([
        ['?','?','?','?'],
        ['?','?','?','?'],
        ['b','?','?','?'],
        ['_','b','?','?'],
    ])

    print('-> Analyzing Wumpus game board...\n')
    ba = BoardAnalyzer(board, gold_probability, wumpus_probability)

    print('-> Saving graph to graph.pdf...\n')
    ba.plot('graph.pdf')

    print('-> Graph probabilites:')
    probs = ba.probabilites()
    for i in range(len(probs)):
        node_name = ba.getNthNodeName(i)
        prob = probs[i]
        if not isinstance(prob, str):
            prob = prob.values()
        print(' * ' + node_name, prob)
