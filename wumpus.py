UNDISCOVERED_FIELD = '?'
BREEZE_FIELD = 'b'
STENCH_FIELD = 's'
BREEZE_N_STENCH_FIELD = 'm'
EMPTY_FIELD = '_'


class Board:

    def __init__(self, board_arr):
        self.width = len(board_arr[0])
        self.height = len(board_arr)
        self.state = board_arr

    def getValue(self, x, y):
        return self.state[y][x]

    def getNeighbourCoords(self, x, y):
        neighbour_coords = []
        potential_coords = [
            (x+1, y),
            (x, y+1),
            (x-1, y),
            (x, y-1),
        ]
        for p_x, p_y in potential_coords:
            if p_x >= 0 and p_x < self.width and p_y >= 0 and p_y < self.height:
                neighbour_coords.append((p_x, p_y))
        return neighbour_coords

    def getFrontiers(self):
        frontiers = set()
        for x in range(self.width):
            for y in range(self.height):
                val = self.getValue(x, y)
                for neigh_x, neigh_y in self.getNeighbourCoords(x, y):
                    neigh_val = self.getValue(neigh_x, neigh_y)
                    if val == UNDISCOVERED_FIELD and neigh_val != UNDISCOVERED_FIELD:
                        frontiers.add((x, y))
                    if val != UNDISCOVERED_FIELD and neigh_val == UNDISCOVERED_FIELD:
                        frontiers.add((neigh_x, neigh_y))
        return frontiers

    def getBreezes(self):
        breezes = set()
        for x in range(self.width):
            for y in range(self.height):
                val = self.getValue(x, y)
                if val == BREEZE_FIELD or val == BREEZE_N_STENCH_FIELD:
                    breezes.add((x, y))
        return breezes

    def getStenches(self):
        breezes = set()
        for x in range(self.width):
            for y in range(self.height):
                val = self.getValue(x, y)
                if val == STENCH_FIELD or val == BREEZE_N_STENCH_FIELD:
                    breezes.add((x, y))
        return breezes
